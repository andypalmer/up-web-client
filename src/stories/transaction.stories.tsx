import type { Meta, StoryObj } from "@storybook/react"
import { Transaction } from "../components/bank/transaction"

const meta: Meta<typeof Transaction> = {
  title: 'Bank/Transaction',
  component: Transaction,
}
export default meta

type Story = StoryObj<typeof Transaction>

export const Default: Story = {
  args: {
    createdAt: "2023-10-23T06:57:00Z",
    otherParty: "Up Bank",
    reference: "Salary",
    amount: { currencyCode: "AUD", amount: "123.45", symbol: "$", },
    tags: [ "tag 1", "tag 2", "Tag A" ],
  },
}
