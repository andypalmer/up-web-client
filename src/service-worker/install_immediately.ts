export function addTo(me: ServiceWorkerGlobalScope) {
  me.addEventListener('install', function(event) {
    event.waitUntil(me.skipWaiting())
  })

  me.addEventListener('activate', function(event) {
    event.waitUntil(me.clients.claim())
  })
}
