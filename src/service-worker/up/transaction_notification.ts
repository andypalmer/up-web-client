import { transactions, accounts, unacknowledgedNotifications } from "../../up/repositories"
import { mapUpTransactionToDisplayTransaction } from "../../mappers/up_to_domain"
import { TransactionId, unacknowledgedTransactions } from "../../domain/bank/transaction"
import { Account as UpAccount } from "../../up/api"

type TransactionEventType = "TRANSACTION_CREATED" | "TRANSACTION_SETTLED" | "TRANSACTION_DELETED"

export async function notifyTransaction(me: ServiceWorkerGlobalScope, event: PushEvent) {
  const somethingEmoji: Record<TransactionEventType, string> = {
    TRANSACTION_CREATED: "🟡",
    TRANSACTION_SETTLED: "🟢",
    TRANSACTION_DELETED: "🔴",
  }

  const payload = event.data?.json()
  const id = payload?.webhook?.data?.relationships?.transaction?.data?.id
  const eventType = payload?.webhook?.data?.attributes?.eventType as TransactionEventType

  await transactions.fetchNew()

  const t = await transactions.get(id)
  if(!t) { return }

  const accountId = t.relationships.account.data.id

  const { lastAcknowledged, currentPushes } = await unacknowledgedNotifications.forAccount(accountId, { id, eventType })

  const transactionsForAccount = (await transactions.forAccount(accountId)).map(t => mapUpTransactionToDisplayTransaction(t))

  const relevantTransactions = unacknowledgedTransactions({
    transactions: transactionsForAccount,
    lastAcknowledged,
    currentPushes: currentPushes.map(pe => pe.id),
  })

  const emojiByTransactionId = Object.fromEntries(currentPushes.map(p => [ p.id, somethingEmoji[p.eventType] ]))
  function emoji(id: TransactionId) {
    return emojiByTransactionId[id] ?? "❓"
  }

  const body = relevantTransactions.map(t => `${emoji(t.id)} ${t.otherParty} ${t.amount.symbol} ${t.amount.amount}`).join("\n")

  const account = await accounts.get(accountId) as UpAccount
  me.registration.showNotification(account?.attributes.displayName, {
    body,
    tag: accountId,
    renotify: true,
    data: {
      accountId,
      latestId: relevantTransactions[0].id,
    },
  })
}

export function notificationClose(event: NotificationEvent) {
  const latestId = event.notification.data?.latestId
  const accountId = event.notification.data?.accountId

  if(!latestId || !accountId) { return }

  unacknowledgedNotifications.reset({ accountId, to: latestId })
}
