import { Avatar, ListItemAvatar, ListItemButton, ListItemText, Typography } from "@mui/material"

import { Account as DisplayAccount } from "../../domain/bank/account"

type AccountProps = {
  selected?: boolean
  account: DisplayAccount
}

export function Account({account, selected=false}: AccountProps) {
  return (
    <ListItemButton title={`${account.name} - ${account.balance.symbol}${account.balance.amount}`} {...{selected}}>
      <ListItemAvatar>
        <Avatar>
          {account.avatar}
        </Avatar>
      </ListItemAvatar>
      <ListItemText
        primary={
          <Typography sx={{ fontSize: "1.2em", paddingLeft: "1rem", paddingRight: "1em", }} color="text.primary" gutterBottom>
            {account.name}
          </Typography>
        }
        secondary={<Typography sx={{ paddingLeft: "1rem", }} color="text.secondary">{account.balance.symbol}{account.balance.amount}</Typography>}
      />
    </ListItemButton>
  )
}
