function urlBase64ToUint8Array(base64String: string): Uint8Array {
  const padding = '='.repeat((4 - base64String.length % 4) % 4)
  const base64 = (base64String + padding)
    .replace(/-/g, '+')
    .replace(/_/g, '/')

  const rawData = atob(base64)
  const outputArray = new Uint8Array(rawData.length)

  for (let i = 0; i < rawData.length; ++i) {
    outputArray[i] = rawData.charCodeAt(i)
  }
  return outputArray
}

export async function pushSubscription({me, publicKey}: {me: ServiceWorkerGlobalScope, publicKey: string}): Promise<PushSubscription> {
  const publicKeyAsUint8 = urlBase64ToUint8Array(publicKey)

  return await me.registration.pushManager.subscribe({ userVisibleOnly: true, applicationServerKey: publicKeyAsUint8 })
}
