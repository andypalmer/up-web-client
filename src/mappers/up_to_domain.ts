import { Account as DisplayAccount } from "../domain/bank/account"
import { Transaction, Account as UpApiAccount, MoneyObject } from "../up/api"
import { Money as DisplayMoneyObject } from "../domain/bank/money"
import { Transaction as DisplayTransaction } from "../domain/bank/transaction"

export function mapUpMoneyObjectToDisplayMoneyObject(input: MoneyObject): DisplayMoneyObject {
  return {
    amount: input.value,
    currencyCode: input.currencyCode,
    symbol: "$",
  }
}

export function mapUpTransactionToDisplayTransaction(input: Transaction): DisplayTransaction {
  return {
    id: input.id,
    otherParty: input.attributes.description,
    amount: mapUpMoneyObjectToDisplayMoneyObject(input.attributes.amount),
    createdAt: input.attributes.createdAt,
    tags: input.relationships.tags.data.map(tag => tag.id),
    reference: input.attributes.message,
    accountId: input.relationships.account.data.id,
  }
}

export function createAvatarFromDisplayName(displayName: string): { avatar: string, name: string } {
  const segmenter = Intl.Segmenter ? splitWithIntlSegmenter : splitByGrapheme
  const segmented = segmenter(displayName)
  return {
    avatar: segmented[0],
    name: segmented[1] !== " " ? displayName : segmented.slice(2).join('').trim()
  }
}

function splitWithIntlSegmenter(input: string): string[] {
  return [...new Intl.Segmenter().segment(input)].map(s => s.segment)
}

export function splitByGrapheme(input: string): string[] {
  // adapted from https://cestoliv.com/blog/how-to-count-emojis-with-javascript/
  const arr = [...input]
  const result: string[] = []
  let currentGrapheme: string[] = []

  for (let c = 0; c < arr.length; c++) {
    currentGrapheme.push(arr[c])
    if (arr[c] != '\u{200D}' && arr[c + 1] != '\u{200D}' && arr[c + 1] != '\u{fe0f}' && arr[c + 1] != '\u{20e3}') {
      result.push(currentGrapheme.join(""))
      currentGrapheme=[]
    }
  }

  return result
}


export function mapUpAccountToDisplayAccount(input: Pick<UpApiAccount, "attributes">): DisplayAccount {
  return {
    ...createAvatarFromDisplayName(input.attributes.displayName),
    balance: mapUpMoneyObjectToDisplayMoneyObject(input.attributes.balance),
  }
}

