import { UpWebhookRepository } from "../up/repositories"
export class UpWebhookRelayClient {
  constructor(private webhookRelayUrl: string) {}
  async register({ webhooks, subscription }: { webhooks: UpWebhookRepository, subscription: PushSubscription }) {
    const transactionWebhookUrl = `${this.webhookRelayUrl}/webhooks/up/transaction`
    const webhook = (await webhooks.all()).find(w => w.attributes.url.startsWith(transactionWebhookUrl)) || await webhooks.create(transactionWebhookUrl)
    if(!webhook) { return }

    return fetch(`${this.webhookRelayUrl}/create_subscription`, {
      method: "POST",
      body: JSON.stringify({webhook, subscription})
    })
  }
}

