if ('serviceWorker' in navigator) {
  const {BASE_URL, MODE} = import.meta.env
  const worker = MODE === "production" ? "service-worker.js" : "dev-sw.js?dev-sw"
  navigator.serviceWorker.register(`${BASE_URL}/${worker}`, { type: "module" })
}
