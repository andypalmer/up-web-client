import { ISODate } from "../domain/time/date"

export type UpResponse<T> = {
  data: T[]
  links: {
    next: string
  }
}

export type Account = {
  type: "accounts",
  id: string,
  attributes: AccountAttributes,
  relationships: unknown,
  links: unknown,
}

export type AccountAttributes = {
  displayName: string,
  balance: MoneyObject,
}

export type MoneyObject = {
  currencyCode: ISO4217
  value: FormattedMoneyString
  valueInBaseUnits: number
}

export type ISO4217 = "AUD"
type FormattedMoneyString = string

export type Transaction = {
  type: "transactions"
  id: string
  attributes: TransactionAttributes
  relationships: {
    account: {
      data: {
        id: string
      }
    }
    tags: {
      data: { id: string }[]
    }
  }
}

export type TransactionAttributes = {
  description: string
  message: string
  amount: MoneyObject
  createdAt: ISODate
}

export type WebhookResource = {
  type: "webhooks"
  id: string
  attributes: WebhookAttributes
}

type WebhookAttributes = {
  url: string
  description?: string
  secretKey?: string
  createdAt: ISODate
}

export class UpClient {
  constructor(private accessToken: string, private base: string = "https://api.up.com.au/api/v1/") { }

  getAccounts(): Promise<UpResponse<Account>> {
    return this.fetch("accounts")
    .catch(() => [])
  }

  getTransactionsForAccount(accountId: string): Promise<UpResponse<Transaction>> {
    return this.fetch(`accounts/${accountId}/transactions`)
    .catch(() => [])
  }

  async *getTransactionsByPage(): AsyncGenerator<Transaction[], void, void> {
    let currentPage = await this.fetch("transactions?page[size]=100")

    while(currentPage.links.next !== null) {
      yield currentPage.data
      currentPage = await this.fetch(currentPage.links.next)
    }

    yield currentPage.data
  }

  async getAllTransactions(): Promise<Transaction[]> {
    const result: Transaction[] = []
    for await (const page of this.getTransactionsByPage()) {
      result.push(...page)
    }

    return result
  }

  getWebhooks(): Promise<UpResponse<WebhookResource>> {
    return this.fetch("webhooks")
  }

  async createWebhook(url: string): Promise<WebhookResource> {
    return fetch(`${this.base}/webhooks`, {
      headers: {
        "Authorization": `Bearer ${this.accessToken}`,
        "Content-Type": "application/json",
      },
      method: "POST",
      body: JSON.stringify({
        data: {
          attributes: {
            url
          }
        }
      }),
    })
    .then(r => r.json())
    .then(r => r.data)
  }

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  private fetch(url: string):Promise<any> {
    const absoluteUrl = new URL(url, this.base)
    return fetch(`${absoluteUrl}`,
      {
        headers: {
          "Authorization": `Bearer ${this.accessToken}`
        },
      },
    )
    .then(r => r.json())
  }
}
