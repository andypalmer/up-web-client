import { DateTime } from "luxon"

import { ISODate } from "../time/date"
import { Money } from "./money"
import { AccountId } from "./account"

export type TransactionId = string

export type Transaction = {
  id: TransactionId
  otherParty: string
  reference: string
  amount: Money
  createdAt: string
  tags: string[]
  accountId: AccountId
}

export type TransactionsByDate = {
  [key: ISODate]: {
    transactions: Transaction[]
  }
}

export function groupByDate(transactions: Transaction[]): TransactionsByDate {
  const result: TransactionsByDate = {}
  transactions.forEach(t => {
    const date = DateTime.fromISO(t.createdAt).toISODate()
    if(!date) { return }

    result[date] = { transactions: ([] as Transaction[]).concat(result[date]?.transactions ?? [], [t]) }
  })
  return result
}

export function unacknowledgedTransactions({ transactions, lastAcknowledged, currentPushes }: { transactions: Transaction[], lastAcknowledged: TransactionId, currentPushes: TransactionId[], }) {
  const unacknowledged = transactions.slice(0, transactions.findIndex(transaction => transaction.id === lastAcknowledged))
  const unacknowledgedIds = unacknowledged.map(transaction => transaction.id)
  const olderTransactionIds = currentPushes.filter(id => !unacknowledgedIds.includes(id))
  const olderTransactions: Transaction[] = olderTransactionIds
    .map(id => transactions.find(transaction => transaction.id === id)
      ?? { id, otherParty: "Not found transaction" } as Transaction
    )

  return unacknowledged.concat(olderTransactions).map(t => ({...t, pushed: currentPushes.includes(t.id), }))
}
