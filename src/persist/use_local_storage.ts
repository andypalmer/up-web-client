import { useState, useEffect, Dispatch, SetStateAction } from 'react';

export function useLocalStorage<T>(key: string, defaultValue: T): [T, Dispatch<SetStateAction<T>>]  {
  function getStorageValue(key: string, defaultValue: unknown): T {
    const saved = localStorage.getItem(key)
    const initial = saved && JSON.parse(saved)
    return initial || defaultValue
  }

  const [value, setValue] = useState<T>(() => {
    return getStorageValue(key, defaultValue)
  })

  useEffect(() => {
    localStorage.setItem(key, JSON.stringify(value))
  }, [key, value])

  return [value, setValue]
}
