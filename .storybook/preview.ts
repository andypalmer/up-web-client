import type { Preview } from "@storybook/react"
import { withMuiTheme, themeToolbar } from "./theme_switcher"

export const globalTypes = { theme: themeToolbar }
export const decorators = [ withMuiTheme ]

const preview: Preview = {
  parameters: {
    actions: { argTypesRegex: "^on[A-Z].*" },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/,
      },
    },
  },
}

export default preview
