export function urlSafeHashcode(input: string): Promise<string> {
  return crypto.subtle.digest('SHA-256', new TextEncoder().encode(input))
    .then(b => btoa(String.fromCharCode(...new Uint8Array(b))))
    .then(result => result.replaceAll("/","_").replaceAll("+","-"))
}
