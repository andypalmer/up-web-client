import { Transaction, groupByDate } from "../../../domain/bank/transaction"

type RelevantTransaction = Partial<Transaction> & Pick<Transaction, "id" | "createdAt">

describe("Group transactions by date", () => {
  it("groups by date", () => {
    const transactions: RelevantTransaction[] = [
      {
        id: "1",
        createdAt: "2023-09-29T17:59:00+10:00",
      },
      {
        id: "2",
        createdAt: "2023-09-29T18:59:00+10:00",
      },
      {
        id: "3",
        createdAt: "2023-09-30T18:59:00+10:00",
      }
    ]

    const result = groupByDate(transactions as Transaction[])

    expect(result).toEqual({
      "2023-09-29": {
        transactions: [
          {
            id: "1",
            createdAt: "2023-09-29T17:59:00+10:00",
          },
          {
            id: "2",
            createdAt: "2023-09-29T18:59:00+10:00",
          },
        ]
      },
      "2023-09-30": {
        transactions: [
          {
            id: "3",
            createdAt: "2023-09-30T18:59:00+10:00",
          }
        ],
      },
    })
  })
})
