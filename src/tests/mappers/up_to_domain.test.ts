import { createAvatarFromDisplayName, mapUpAccountToDisplayAccount, mapUpTransactionToDisplayTransaction, splitByGrapheme } from "../../mappers/up_to_domain"
import { Transaction } from "../../up/api"

describe("extracting avatars from account names", () => {
  it("doesn't remove the initial character when part of a multicharacter word", () => {
    const displayName = "Testing"
    const expected: { avatar: string, name: string} = {
      avatar: "T",
      name: "Testing",
    }
    const actual = createAvatarFromDisplayName(displayName)

    expect(actual).toEqual(expected)
  })

  it("does remove the initial character when character is solitary", () => {
    const displayName = "💰 Savings"
    const expected: { avatar: string, name: string} = {
      avatar: "💰",
      name: "Savings",
    }
    const actual = createAvatarFromDisplayName(displayName)

    expect(actual).toEqual(expected)
  })

  it("handles single characters built using zero-width joiners", () => {
    const displayName = "♟️ Tax"
    const expected: { avatar: string, name: string} = {
      avatar: "♟️",
      name: "Tax",
    }
    const actual = createAvatarFromDisplayName(displayName)

    expect(actual).toEqual(expected)
  })
})

describe("segmenting emojis without Intl.Segmenter", () => {
  it("will correctly segment a single width emoji", () => {
    const displayName = "💰 Savings"

    const expected = ["💰", " ", "S", "a", "v", "i", "n", "g", "s"]
    const actual = splitByGrapheme(displayName)

    expect(actual).toEqual(expected)
  })

  it("will correctly segment a composite emoji", () => {
    const displayName = "♟️ Tax"

    const expected = ["♟️", " ", "T", "a", "x"]
    const actual = splitByGrapheme(displayName)

    expect(actual).toEqual(expected)
  })
})

describe("mapping Up Account to UI domain", () => {
  it("maps an account without an icon", () => {
    const up_response: Parameters<typeof mapUpAccountToDisplayAccount>[0] = {
      attributes: {
        displayName: "Testing",
        balance: {
          currencyCode: "AUD",
          value: "123.45",
          valueInBaseUnits: 12345,
        },
      },
    }
    const result = mapUpAccountToDisplayAccount(up_response)
    const expected = {
      avatar: "T",
      name: "Testing",
      balance: {
        amount: "123.45",
        currencyCode: "AUD",
        symbol: "$",
      },
    }

    expect(result).toEqual(expected)
  })

  it("maps an account with an icon", () => {
    const up_response: Parameters<typeof mapUpAccountToDisplayAccount>[0] = {
      attributes: {
        displayName: "💰 Savings",
        balance: {
          currencyCode: "AUD",
          value: "123.45",
          valueInBaseUnits: 12345,
        },
      },
    }
    const result = mapUpAccountToDisplayAccount(up_response)
    const expected = {
      avatar: "💰",
      name: "Savings",
      balance: {
        amount: "123.45",
        currencyCode: "AUD",
        symbol: "$",
      },
    }

    expect(result).toEqual(expected)
  })
})

describe("mapping Up Transaction to UI domain", () => {
  const upTransaction: Transaction = {
    type: "transactions",
    id: "irrelevant-transaction",
    attributes: {
      description: "This is a description",
      message: "This is a message",
      createdAt: "2023-08-09T06:53:00Z",
      amount: {
        currencyCode: "AUD",
        value: "123.45",
        valueInBaseUnits: 12345,
      },
    },
    relationships: {
      account: {
        data: {
          id: "irrelevant-account"
        },
      },
      tags: {
        data: []
      },
    },
  }

  it("extracts the core transaction information", () => {
    const expected = {
      id: "irrelevant-transaction",
      accountId: "irrelevant-account",
      otherParty: "This is a description",
      reference: "This is a message",
      createdAt: "2023-08-09T06:53:00Z",
      amount: {
        amount: "123.45",
        currencyCode: "AUD",
        symbol: "$",
      },
      tags: [],
    }

    const result = mapUpTransactionToDisplayTransaction(upTransaction)

    expect(result).toEqual(expected)
  })


  it("extracts a list of tags", () => {
    const upTransactionWithTags = {
      ...upTransaction,
      relationships: {
        ...upTransaction.relationships,
        tags: {
          data: [
            { id: "tag 1", },
            { id: "tag 2", },
            { id: "Tag A", },
          ],
        },
      },
    }

    const expected = {
      tags: [ "tag 1", "tag 2", "Tag A" ],
    }

    const result = mapUpTransactionToDisplayTransaction(upTransactionWithTags)

    expect(result.tags).toEqual(expected.tags)
  })
})
