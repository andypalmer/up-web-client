import { openDB, DBSchema, IDBPDatabase, OpenDBCallbacks } from "idb"
import { Transaction, Account as UpApiAccount, Transaction as UpApiTransaction, UpClient, WebhookResource } from "../up/api"
import { urlSafeHashcode } from "../crypto/url_safe_hash"
import { AccountId } from "../domain/bank/account"
import { TransactionId } from "../domain/bank/transaction"

type HashedAccountId = {
  id: string
  hash: string
}

type PushAndEvent = {
  id: TransactionId
  eventType: TransactionEventType
}

type AcknowledgmentInfo = {
  accountId: AccountId
  currentPushes: PushAndEvent[]
  lastAcknowledged: TransactionId
}

// FIXME 202310201551 ap - This should go in a domain interface
type TransactionEventType = "TRANSACTION_CREATED" | "TRANSACTION_SETTLED" | "TRANSACTION_DELETED"

interface UpSchema extends DBSchema {
  config: {
    key: string
    value: unknown
  },
  accounts: {
    key: string
    value: UpApiAccount
  },
  transactions: {
    key: string
    value: UpApiTransaction
    indexes: { 'idx_accountid': string }
  },
  hashedAccounts: {
    key: string
    value: HashedAccountId
  },
  "ext-unacknowledgedNotifications": {
    key: AccountId
    value: AcknowledgmentInfo
  },
}

class UpAccountRepository {
  constructor(private db: Promise<IDBPDatabase<UpSchema>>) {}

  async get(id: string): Promise<UpApiAccount | undefined> {
    return (await this.db).get("accounts", id)
  }

  async all(): Promise<UpApiAccount[]> {
    const cached = await (await this.db).getAll("accounts")
    if(cached.length !== 0) { return cached }

    return this.refresh()
  }

  async byHash(hash: string): Promise<UpApiAccount | undefined> {
    const db = await this.db

    const hashed = await db.get("hashedAccounts", hash)
    if(!hashed) { return undefined }

    return this.get(hashed.id)
  }

  async refresh(): Promise<UpApiAccount[]> {
    const accounts = await (await up()).getAccounts()
    if((accounts as unknown as {errors: string[]}).errors) { return [] }
    await Promise.all(accounts.data.map(async acc => {
      const db = await this.db
      await db.put("accounts", acc)
      await db.put("hashedAccounts", { id: acc.id, hash: await urlSafeHashcode(acc.id) })
    }))

    return accounts.data
  }
}

class UpTransactionRepository {
  constructor(private db: Promise<IDBPDatabase<UpSchema>>) {}

  async get(id: string): Promise<Transaction | undefined> {
    return (await this.db).get("transactions", id)
  }

  async forAccount(accountId: string): Promise<Transaction[]> {
    const transactions = (await (await this.db).getAllFromIndex("transactions", "idx_accountid", accountId))
      .sort((a,b) => Date.parse(b.attributes.createdAt) - Date.parse(a.attributes.createdAt))

    return transactions
  }

  async fetchAll(): Promise<Transaction[]> {
    const transactions = await (await up()).getAllTransactions()

    await this.put(...transactions)

    return transactions
  }

  async fetchNew(): Promise<Transaction[]> {
    const newTransactions: Transaction[] = []
    for await (const transactions of (await up()).getTransactionsByPage()) {
      const tx = (await this.db).transaction("transactions", "readonly")
      const transactionsInDatabase: [Transaction|undefined, Transaction][] = await Promise.all(transactions.map(async t => [await tx.store.get(t.id), t]))

      const unfoundTransactions = transactionsInDatabase
        .filter(t => t[0] === undefined)
        .map(t => t[1])

      await this.put(...transactions)
      newTransactions.push(...unfoundTransactions)

      if(unfoundTransactions.length !== transactions.length) { break }
    }

    return newTransactions
  }

  async put(...transactions: Transaction[]): Promise<Transaction[]> {
    const tx = (await this.db).transaction("transactions", "readwrite")

    await Promise.all([...transactions.map(t => tx.store.put(t)), tx.done])

    return transactions
  }
}

export class UpWebhookRepository {
  constructor(private db: Promise<IDBPDatabase<UpSchema>>) {this.db} // TODO 20230922 ap - this is only temporary until we actually store webhooks in the cache

  async all(): Promise<WebhookResource[]> {
    const webhooks = await (await up()).getWebhooks()
    if((webhooks as unknown as {errors: string[]}).errors) { return [] }

    return webhooks.data
  }

  async create(url: string): Promise<WebhookResource> {
    return await (await up()).createWebhook(url)
  }
}

export class UnacknowledgedNotificationRepository {
  constructor(private db: Promise<IDBPDatabase<UpSchema>>) {}

  async forAccount(accountId: AccountId, currentPush: PushAndEvent ): Promise<AcknowledgmentInfo> {
    const result = (await (await this.db).get("ext-unacknowledgedNotifications", accountId))
      || { accountId, currentPushes: [], lastAcknowledged: currentPush.id, }

    result.currentPushes.push(currentPush)
    this.db.then(db => db.put("ext-unacknowledgedNotifications", result))

    return result
  }

  async reset({ accountId, to }: { accountId: AccountId, to: TransactionId }) {
    return await (await this.db).put("ext-unacknowledgedNotifications", { accountId, currentPushes: [], lastAcknowledged: to, })
  }
}

type UpgradeParameters<Schema extends DBSchema> = Parameters<Required<OpenDBCallbacks<Schema>>['upgrade']>

type MigrationParameters<Schema extends DBSchema, T extends UpgradeParameters<Schema> = UpgradeParameters<Schema>> = {
  db: T[0]
  oldVersion: T[1]
  newVersion: T[2]
  transaction: T[3]
  event: T[4]
}

type Migration<Schema extends DBSchema> = (params: MigrationParameters<Schema>) => void

const migrations: Map<number, Migration<UpSchema>> = new Map([
  [ 2, ({db}) => { db.createObjectStore("config") }],

  [ 202309141200, ({db}) => {
    db.createObjectStore("accounts", { keyPath: "id" })
    db.createObjectStore("transactions", { keyPath: "id" })
  }],

  [ 202309141732, ({db}) => {
    db.createObjectStore("hashedAccounts", { keyPath: "hash" })
  }],

  [ 202309271846, ({transaction}) => {
    const store = transaction.objectStore("transactions")
    store.createIndex("idx_accountid", "relationships.account.data.id")
  }],

  [ 202310201557, ({db}) => {
    db.createObjectStore("ext-unacknowledgedNotifications", { keyPath: "accountId" })
  }],

])

const dbPromise = openDB<UpSchema>("au.com.up", Math.max(...migrations.keys()), {
  upgrade(db, oldVersion, newVersion, transaction, event) {
    [...migrations.entries()].forEach(([key, migration]) => {
      if(oldVersion < key) { migration({db, oldVersion, newVersion, transaction, event}) }
    })
  }
})

async function up():Promise<UpClient> {
  return dbPromise.then(async (db) => {
    const accessToken = await db.get("config", "accessToken") as string
    return new UpClient(accessToken)
  })
}

export const accounts = new UpAccountRepository(dbPromise)
export const transactions = new UpTransactionRepository(dbPromise)
export const webhooks = new UpWebhookRepository(dbPromise)
export const unacknowledgedNotifications = new UnacknowledgedNotificationRepository(dbPromise)

export async function updateAccessToken(accessToken: string) {
  const db = await dbPromise
  if(accessToken === await db.get("config", "accessToken")) { return }

  await db.put("config", accessToken, "accessToken")
  await db.clear("accounts")
  await db.clear("transactions")
}
