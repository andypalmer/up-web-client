import type { Meta, StoryObj } from "@storybook/react"
import { TransactionsByDate } from "../components/bank/transactions_by_date"
import { Transaction } from "../domain/bank/transaction"

const meta: Meta<typeof TransactionsByDate> = {
  title: 'Bank/TransactionsByDate',
  component: TransactionsByDate,
}
export default meta

type Story = StoryObj<typeof TransactionsByDate>

export const Default: Story = {
  args: {
    date: "2023-10-04",
    transactions: [
      {
        id: "id-1",
        createdAt: "2023-10-23T06:57:00Z",
        otherParty: "Up Bank",
        reference: "Salary",
        amount: { currencyCode: "AUD", amount: "123.45", symbol: "$", },
        tags: [ "tag 1", "tag 2", "Tag A" ],
      } as Transaction
    ],
  },
}
