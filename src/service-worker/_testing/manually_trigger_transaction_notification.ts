export async function manuallyTriggerTransactionNotification(data: Record<string, unknown>) {
  await fetch(`${import.meta.env.VITE_WEBHOOK_RELAY_BASE_URL}/webhooks/up/transaction`, {
    method: "POST",
    body: JSON.stringify({
      "data": {
        "type": "webhook-events",
        "id": "2ca61640-e795-4e1a-adfd-afa1883b419d",
        "attributes": {
          "eventType": "TRANSACTION_CREATED",
          "createdAt": "2023-09-06T12:47:10+10:00"
        },
        "relationships": {
          "webhook": {
            "data": {
              "type": "webhooks",
              "id": "827bae67-ff0f-4dc1-aa8a-4207e731ea19"
            },
            "links": {
              "related": "https://api.up.com.au/api/v1/webhooks/827bae67-ff0f-4dc1-aa8a-4207e731ea19"
            }
          },
          "transaction": {
            "data": {
              "type": "transactions",
              "id": data?.id,
            },
            "links": {
              "related": "https://api.up.com.au/api/v1/transactions/8f2a3092-45b7-409a-9d84-07718a6785b5"
            }
          }
        }
      }
    })
  })
}
