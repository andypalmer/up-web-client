import { Dispatch, SetStateAction } from "react"
import { Button, TextField } from "@mui/material"
import Grid from "@mui/material/Unstable_Grid2"

function createSubscription(serviceWorker: ServiceWorker | null | undefined ) {
  serviceWorker?.postMessage({ type: "createSubscription", })
}

type ConfigurationBarProps = {
  upAccessToken: string
  setUpAccessToken: Dispatch<SetStateAction<string>>
  serviceWorker: ServiceWorker | null | undefined
}

export function ConfigurationBar({upAccessToken, setUpAccessToken, serviceWorker}: ConfigurationBarProps) {
  return (
    <Grid container sx={{ marginTop: 2, marginLeft: 2}} spacing={2} className="App">
      <Grid xs={8}>
        <TextField
          id="outlined-basic"
          label="Up Access Token"
          variant="outlined"
          type="password"
          fullWidth={true}
          value={upAccessToken}
          onChange={(e) => { setUpAccessToken(e.target.value) } }
        />
      </Grid>
      <Grid xs={2}>
        <Button variant="contained" onClick={() => { setUpAccessToken("") }}>Clear</Button>
      </Grid>
      <Grid xs={2}>
        <Button variant="contained" onClick={async () => { Notification && await Notification.requestPermission(); createSubscription(serviceWorker) }}>Enable Notifications</Button>
      </Grid>
    </Grid>
  )
}

