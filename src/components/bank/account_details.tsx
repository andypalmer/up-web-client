import { useEffect, useState } from "react"
import { useParams } from "react-router-dom"
import { Typography } from "@mui/material"

import { TransactionsByDate as ByDate } from "../../domain/bank/transaction"
import { TransactionsByDate } from "./transactions_by_date"

type AccountDetailsProps = {
  serviceWorker: ServiceWorker | null | undefined
}

export function AccountDetails({serviceWorker}: AccountDetailsProps) {
  const { accountHash } = useParams()
  const [name, setName] = useState<string>("unknown")
  const [transactionsByDate, setTransactionsByDate] = useState<ByDate>({})

  function updateTransactions(event: MessageEvent) {
    if(event.data?.type !== "getTransactionsForAccountReply") { return }
    setName(event.data.name)
    setTransactionsByDate(event.data.transactionsByDate)
  }

  useEffect(() => {
    navigator.serviceWorker.addEventListener("message", updateTransactions)
    async function requestTransactions() {
      serviceWorker?.postMessage({ type: "getTransactionsForAccount", accountHash, })
    }
    requestTransactions()

    return () => { navigator.serviceWorker.removeEventListener("message", updateTransactions) }
  }, [accountHash, serviceWorker])

  return (
    <>
      <h1>Account Page for {name}</h1>
      <Typography variant="h5"></Typography>
      { Object.entries(transactionsByDate).map(([date, transactions]) => <TransactionsByDate key={date} {...{date, ...transactions}} />) }
    </>
  )
}
