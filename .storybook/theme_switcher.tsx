import { createTheme, CssBaseline, PaletteMode, Theme, ThemeProvider } from "@mui/material"
import { useMemo } from "react"

export const themeToolbar = {
  name: "Theme",
  title: "Theme",
  description: "Theme for your components",
  defaultValue: "light",
  toolbar: {
    icon: "paintbrush",
    dynamicTitle: true,
    items: [
      { value: "light", left: "☀️", title: "Light mode" },
      { value: "dark", left: "🌙", title: "Dark mode" },
    ],
  },
}

function theme(paletteName: PaletteMode): Theme {
  return createTheme({ palette: { mode: paletteName, }, })
}

const THEMES = {
  light: theme("light"),
  dark: theme("dark"),
}

export const withMuiTheme = (Story: any, context: any) => {
  const { theme: themeKey }:{theme: "light" | "dark" } = context.globals;

  const theme = useMemo(() => THEMES[themeKey] || THEMES["light"], [themeKey]);

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <Story />
    </ThemeProvider>
  )
}
