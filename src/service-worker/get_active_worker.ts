function currentWorkerRepository() {
  let current: ServiceWorker | null | undefined = navigator.serviceWorker.controller

  navigator.serviceWorker.addEventListener("controllerchange", async (event) => {
    current = (event.target as unknown as ServiceWorkerContainer).controller
  })

  async function getCurrent() {
    current = (await navigator.serviceWorker.getRegistrations()).find(r => r.active)?.active
  }
  getCurrent()

  return function() {
    return current
  }
}

export const activeServiceWorker = currentWorkerRepository()
