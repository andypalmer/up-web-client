import { defineConfig } from "vite"
import react from "@vitejs/plugin-react"
import { VitePWA } from "vite-plugin-pwa"

// https://vitejs.dev/config/
export default defineConfig({
  base: "/up-web-client", // TODO 20230816 ap - can/should we set this via env variable? See also <base> in index.html
  root: "src",
  build: {
    outDir: "../dist",
  },
  plugins: [
    react(),
    VitePWA({
      srcDir: "",
      filename: "service-worker.ts",
      strategies: "injectManifest",
      injectRegister: false,
      manifest: false,
      injectManifest: {
        injectionPoint: null,
      },
      devOptions: {
        enabled: true,
      },
    }),
  ],
})
