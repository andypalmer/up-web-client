import { Transaction, unacknowledgedTransactions } from "../../../domain/bank/transaction"

describe("consolidating notifications by account", () => {
  const transactions: Transaction[] = [
    { id: "4", },
    { id: "3", },
    { id: "2", },
    { id: "1", },
  ] as Transaction[]

  const lastAcknowledged = "1"

  it("does not include transactions that have already been acknowledged", () => {
    const currentPushes = [ "4", "3", "2" ]
    const expected = [
      { id: "4", pushed: true, },
      { id: "3", pushed: true, },
      { id: "2", pushed: true, },
    ]

    const result = unacknowledgedTransactions({ transactions, lastAcknowledged, currentPushes, })

    expect(result).toEqual(expected)
  })

  it("annotates transactions that we haven't received a push for", () => {
    const currentPushes = [ "3" ]
    const expected = [
      { id: "4", pushed: false, },
      { id: "3", pushed: true, },
      { id: "2", pushed: false, },
    ]

    const result = unacknowledgedTransactions({ transactions, lastAcknowledged, currentPushes, })

    expect(result).toEqual(expected)
  })

  it("includes transactions older than the lastAcknowledged if they're specifically pushed", () => {
    const currentPushes = [ "4", "1" ]
    const lastAcknowledged = "2"
    const expected = [
      { id: "4", pushed: true, },
      { id: "3", pushed: false, },
      { id: "1", pushed: true, },
    ]

    const result = unacknowledgedTransactions({ transactions, lastAcknowledged, currentPushes, })

    expect(result).toEqual(expected)
  })

  it("tells us about not found transactions", () => {
    const currentPushes = [ "4", "NotFound" ]
    const lastAcknowledged = "2"
    const expected = [
      { id: "4", pushed: true, },
      { id: "3", pushed: false, },
      { id: "NotFound", otherParty: "Not found transaction", pushed: true, },
    ]

    const result = unacknowledgedTransactions({ transactions, lastAcknowledged, currentPushes, })

    expect(result).toEqual(expected)
  })
})
