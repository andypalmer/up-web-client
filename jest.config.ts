import type { Config } from "@jest/types"

const config: Config.InitialOptions = {
  preset: "ts-jest",
  testEnvironment: "jsdom",
  verbose: true,
}
export default config
/*
module.exports = {
  preset: 'ts-jest/presets/default-esm',
  moduleNameMapper: { '^(\\.{1,2}/.*)\\.js$': '$1', },
  transform: { '^.+\\.tsx?$': [ 'ts-jest', { useESM: true, }, ], },
  testEnvironment: 'node',
};
*/
