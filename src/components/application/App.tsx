import { useEffect, useMemo, useState } from "react"
import { createBrowserRouter, Link, Outlet, RouterProvider, useParams } from "react-router-dom"

import { createTheme, CssBaseline, Grid, List, ThemeProvider, useMediaQuery } from "@mui/material"

import { useLocalStorage } from "../../persist/use_local_storage"
import { activeServiceWorker } from "../../service-worker/get_active_worker"

import { MiniDrawerLayout } from "../layout/mini_drawer"
import { ConfigurationBar } from "../application/configuration_bar"
import { Account } from "../bank/account"
import { AccountDetails } from "../bank/account_details"
import { AccountWithHash } from "../../domain/bank/account"


function AccountChoice({accounts}:{accounts: AccountWithHash[]}) {
  const { accountHash } = useParams()
  return (
    <List>
      { accounts.map(account => (
        <Grid xs={12} item key={account.hash}>
          <Link style={{textDecoration: "none"}} to={`/accounts/${account.hash}`}>
            <Account {...{account, selected: account.hash === accountHash }} />
          </Link>
        </Grid>
        ))
      }
    </List>
  )
}


function App() {
  const [upAccessToken, setUpAccessToken] = useLocalStorage("au.com.up.accessToken", "")
  const [accounts, setAccounts] = useState<AccountWithHash[]>([])
  const [serviceWorker, setServiceWorker] = useState<ServiceWorker | null | undefined>(activeServiceWorker)

  useEffect(() => {
    async function updateToken() {
      serviceWorker?.postMessage({ type: "setUpAccessToken", upAccessToken, })
    }
    updateToken()

    if(!upAccessToken) {
      setAccounts([])
      return
    }

    async function refreshAccounts() {
      serviceWorker?.postMessage({ type: "getAccounts", })
    }
    refreshAccounts()

  }, [upAccessToken, serviceWorker])

  function updateAccounts(event: MessageEvent) {
    if(event.data?.type !== "getAccountsReply") { return }
    setAccounts(event.data.accounts)
  }

  useEffect(() => {
    navigator.serviceWorker.addEventListener("message", updateAccounts)

    return () => { navigator.serviceWorker.removeEventListener("message", updateAccounts) }
  })

  useEffect(() => {
    function updateServiceWorker() {
      setServiceWorker(activeServiceWorker())
    }
    navigator.serviceWorker.addEventListener("controllerchange", updateServiceWorker)

    return () => { navigator.serviceWorker.removeEventListener("controllerchange", updateServiceWorker) }
  })

  const basename = document.querySelector('base')?.getAttribute('href') ?? '/'
  const router = createBrowserRouter(
    [
      {
        path: "/",
        element: <MiniDrawerLayout {...{
          appBar: <ConfigurationBar {...{upAccessToken, setUpAccessToken, serviceWorker}} />,
          drawer: <AccountChoice {...{accounts}} />,
          drawerWidth: 640,
          mainContent: <Outlet />,
        }} />,
        children: [
          {
            path: "/accounts/:accountHash",
            element: <AccountDetails {...{serviceWorker}} />,
          },
        ],
      },
    ],
    { basename },
  )

  const prefersDarkMode = useMediaQuery('(prefers-color-scheme: dark)');

  const theme = useMemo(
    () =>
      createTheme({
        palette: {
          mode: prefersDarkMode ? 'dark' : 'light',
        },
      }),
    [prefersDarkMode],
  );

  return (
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <RouterProvider router={router} />
    </ThemeProvider>
  )
}

export default App
