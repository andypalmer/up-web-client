import { AccountWithHash } from "../domain/bank/account"
import { Transaction, TransactionsByDate, groupByDate } from "../domain/bank/transaction"
import { urlSafeHashcode } from "../crypto/url_safe_hash"
import { mapUpAccountToDisplayAccount, mapUpTransactionToDisplayTransaction } from "../mappers/up_to_domain"
import { accounts as _accounts, transactions as _transactions, webhooks as _webhooks, updateAccessToken } from "../up/repositories"
import { UpWebhookRelayClient } from "./up_webhook_relay"
import { pushSubscription } from "./push/subscription"

import { manuallyTriggerTransactionNotification } from "./_testing/manually_trigger_transaction_notification"
import { notifyTransaction, notificationClose } from "./up/transaction_notification"

export async function addTo(me: ServiceWorkerGlobalScope) {
  me.addEventListener("message", handleMessage)
  me.addEventListener("push", handlePush)
  me.addEventListener("notificationclose", notificationClose)

 async function handleMessage(event: ExtendableMessageEvent) {
     // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handlers: Record<string, (data: Record<string, unknown>) => any> = {
      setUpAccessToken,
      getAccounts,
      getTransactionsForAccount,
      createSubscription,
      notifyTransaction: manuallyTriggerTransactionNotification,
    }

    const handler = handlers[event.data?.type]
    if(!handler) { return }

    me.clients.matchAll({includeUncontrolled: true, type: "window", })
      .then(async clients => {
        const sender = clients.find(client => client.id === (event.source as unknown as WindowClient)?.id)
        sender?.postMessage({ type: `${event.data?.type}Reply`, ...(await handler(event.data))})
      })
  }

  async function handlePush(event: PushEvent) {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const handlers: Record<string, (me: ServiceWorkerGlobalScope, event: PushEvent) => any> = {
      notifyTransaction,
    }
    const payload = event.data?.json()
    const handler = handlers[payload.type]
    if(!handler) { return } // TODO 20230928 ap - This should be the default notification (e.g. a silent notification saying that something changed)

    event.waitUntil(handler(me, event))
  }

  async function setUpAccessToken(data: Record<string, unknown>) {
    const accessToken: string = data?.upAccessToken as string
    if(!accessToken) { return }

    await updateAccessToken(accessToken)
  }

  async function getAccounts() {
    const accounts = await _accounts.all()

    const annotatedAccounts: AccountWithHash[] = await Promise.all(accounts.map(async (a) => ({...mapUpAccountToDisplayAccount(a), id: a.id, hash: await urlSafeHashcode(a.id)})))

    return { accounts: annotatedAccounts }
  }

  async function getTransactionsForAccount(data: Record<string, unknown>): Promise<{accountHash: string, name: string, transactions: Transaction[], transactionsByDate: TransactionsByDate }> {
    const accountHash = data?.accountHash as string
    const account = await _accounts.byHash(accountHash)
    if(!account) return { accountHash, name: "Unknown account", transactions: [], transactionsByDate: {}, }

    const transactions = (await _transactions.forAccount(account.id)).map(transaction => mapUpTransactionToDisplayTransaction(transaction))
    const transactionsByDate = groupByDate(transactions)
    return { accountHash, name: account.attributes.displayName, transactions, transactionsByDate }
  }

  async function createSubscription() {
    const publicKey = "BA955izP8UfYNBjm6SBIKUztud-RT-NhljJ3oIzLHBydLvk-sdTihWC3UkHnfmaieGj-bPA9gmPrJ4wqKmkp_VY"
    const subscription = await pushSubscription({me, publicKey})
    const relayClient = new UpWebhookRelayClient(import.meta.env.VITE_WEBHOOK_RELAY_BASE_URL)

    await relayClient.register({ webhooks: _webhooks, subscription })
  }
}
