/// <reference lib="webworker" />
import * as Up from "./service-worker/up_api"
import * as ImmediateInstall from "./service-worker/install_immediately"

const me: ServiceWorkerGlobalScope = self as unknown as ServiceWorkerGlobalScope
[Up, ImmediateInstall].forEach(library => library.addTo(me))
