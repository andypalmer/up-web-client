import { Money } from "./money"

export type Account = {
  avatar: string
  name: string
  balance: Money
}

export type AccountId = string

export type AccountWithHash = Account & { id: AccountId, hash: string }
