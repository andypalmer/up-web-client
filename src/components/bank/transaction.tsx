import { Card, CardContent, Chip, Typography } from "@mui/material"
import Grid from "@mui/material/Unstable_Grid2"
import { DateTime } from "luxon"

import { Transaction as TransactionProps } from "../../domain/bank/transaction"
import { Money } from "./money"

export function Transaction({ otherParty, reference, amount, tags, createdAt }: TransactionProps) {
  return (
    <Card variant="outlined">
      <CardContent>
        <Grid container>
          <Grid xs={8}>
            <Typography sx={{fontSize: "0.7em"}} color="text.secondary">{DateTime.fromISO(createdAt).toFormat("HH:mm")}</Typography>
            <Typography sx={{fontSize: "1.2em"}}>{otherParty}</Typography>
            <Typography>{reference}</Typography>
          </Grid>
          <Grid xs={3}>
            <Money {...amount} />
          </Grid>
          <Grid container xs={12} spacing={0.5}>
            {tags.map(tag => <Grid key={tag}><Chip label={tag} /></Grid>)}
          </Grid>
        </Grid>
      </CardContent>
    </Card>
  )
}
