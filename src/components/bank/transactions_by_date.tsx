import { DateTime } from "luxon"
import { Card, CardContent, CardHeader } from "@mui/material"

import { Transaction as DomainTransaction } from "../../domain/bank/transaction"
import { ISODate } from "../../domain/time/date"
import { Transaction } from "./transaction"

type TransactionsByDateProps = {
  date: ISODate,
  transactions: DomainTransaction[]
}

export function TransactionsByDate({ date, transactions }: TransactionsByDateProps) {
  const day = DateTime.fromISO(date).toLocaleString(DateTime.DATE_HUGE)
  return (
    <Card sx={{marginBottom: "0.3em"}}>
      <CardHeader title={day} />
      <CardContent>
        { transactions.map(t => <Transaction key={t.id} {...t} />) }
      </CardContent>
    </Card>
  )
}
