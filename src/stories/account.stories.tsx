import {Typography} from "@mui/material"
import type { Meta, StoryObj } from "@storybook/react"

import { Account } from "../components/bank/account"
import { Account as DisplayAccount } from "../domain/bank/account"

const meta: Meta<typeof Account> = {
  title: 'Bank/Account',
  component: Account,
}
export default meta

type Story = StoryObj<typeof Account>

function addSelectedToDescription(accountObject: DisplayAccount, selected: boolean) {
  return {...accountObject, name: `${accountObject.name} (Selected: ${selected})`}
}

export const Default: Story = {
  render: (args) => {
    return (
      <>
        {[false, true].map((selected) => (
          <Typography key={`accountSelected-${selected}`} sx={{ width: "500px" }}>
            <Account {...{account: addSelectedToDescription(args.account, selected), selected}} />
          </Typography>
        ))}
      </>
    )
  },
  args: {
    account: {
      avatar: "A",
      name: "Account Name",
      balance: { currencyCode: "AUD", amount: "123.45", symbol: "$", },
    } as DisplayAccount,
  },
}
