import { Typography } from "@mui/material"
import Grid from "@mui/material/Unstable_Grid2"

import { Money as MoneyObject } from "../../domain/bank/money"

export function Money({symbol, amount, currencyCode}: MoneyObject) {
  return (
    <Grid container>
      <Grid xs={3} alignItems="center" justifyContent="center">
        <Typography sx={{ fontSize: "2em", }}>{symbol}</Typography>
        <Typography sx={{ fontSize: "0.8em", }}>{currencyCode}</Typography>
      </Grid>
      <Grid xs={9}>
        <Typography sx={{ fontSize: "1.5em" }}>
          {amount}
        </Typography>
      </Grid>
    </Grid>
  )
}
